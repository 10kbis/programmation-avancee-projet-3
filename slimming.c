#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include "slimming.h"


typedef struct {
    size_t width;
    size_t height;
    int * data;
} matrix_t;


static matrix_t * createMatrix(size_t width, size_t height) {
    if (width == 0 || height == 0) {
        return NULL;
    }

    matrix_t * m = malloc(sizeof(matrix_t));

    if (!m) {
        return NULL;
    }

    m->width = width;
    m->height = height;
    m->data = malloc(sizeof(int) * width * height);

    if(!m->data) {
        free(m);
        return NULL;
    }

    for (size_t i = 0; i < width * height; i++) {
        m->data[i] = -1;
    }

    return m;
}

static void freeMatrix(const matrix_t * m) {
    free(m->data);
    free((void *)m);
}

static void setElementMatrix(const matrix_t * m, size_t x, size_t y, int value) {
    assert(y < m->height);
    assert(x < m->width);
    m->data[y * m->width + x] = value;
}

static int getElementMatrix(const matrix_t * m, size_t x, size_t y) {
    assert(y < m->height);
    assert(x < m->width);
    return m->data[y * m->width + x];
}

static PNMPixel getPixelPNM(const PNMImage * image, size_t x, size_t y) {
    assert(y < image->height);
    assert(x < image->width);
    return image->data[y * image->width + x];
}

static void setPixelImagePNM(const PNMImage * image, size_t x, size_t y, PNMPixel pixel) {
    assert(y < image->height);
    assert(x < image->width);
    image->data[y * image->width + x] = pixel;
}

static int getEnergyPixel(const PNMImage * image, size_t x, size_t y) {
    int energy = 0;
    PNMPixel up, down, left, right;

    if (y > 0) {
        up = getPixelPNM(image, x, y - 1);
    } else {
        up = getPixelPNM(image, x, y);
    }

    if (y < image->height - 1) {
        down = getPixelPNM(image, x, y + 1);
    } else {
        down = getPixelPNM(image, x, y);
    }

    if (x > 0) {
        left = getPixelPNM(image, x - 1, y);
    } else {
        left = getPixelPNM(image, x, y);
    }

    if (x < image->width - 1) {
        right = getPixelPNM(image, x + 1, y);
    } else {
        right = getPixelPNM(image, x, y);
    }

    energy += abs(left.red - right.red) + abs(up.red - down.red);
    energy += abs(left.green - right.green) + abs(up.green - down.green);
    energy += abs(left.blue - right.blue) + abs(up.blue - down.blue);
    
    return energy / 2;
}

static matrix_t * buildEnergyMatrix(const PNMImage * image) {
    matrix_t * energy = createMatrix(image->width, image->height);

    if (!energy) {
        return NULL;
    }

    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            setElementMatrix(energy, x, y, getEnergyPixel(image, x, y));
        }
    }
    
    return energy;
}

static int min(int a, int b) {
    return a > b ? b : a;
}

static int computeCumulativeEnergyPath(const matrix_t * cumulative, const matrix_t * energy, size_t x, size_t y, size_t real_width) {
    int value = getElementMatrix(cumulative, x, y);
    if (value != -1) {
        return value;
    }
    
    value = getElementMatrix(energy, x, y);

    if (y != cumulative->height - 1) {
        int min_value = computeCumulativeEnergyPath(cumulative, energy, x, y + 1, real_width);
        if (x > 0) {
            min_value = min(min_value, computeCumulativeEnergyPath(cumulative, energy, x - 1, y + 1, real_width));
        }
        if (x < real_width - 1) {
            min_value = min(min_value, computeCumulativeEnergyPath(cumulative, energy, x + 1, y + 1, real_width));
        }
        value += min_value;
    }

    setElementMatrix(cumulative, x, y, value);

    return value;
}

static matrix_t * buildCumulativeEnergyMatrix(const matrix_t * energy) {
    matrix_t * cumulative = createMatrix(energy->width, energy->height);
    
    for (size_t x = 0; x < energy->width; x++) {
        computeCumulativeEnergyPath(cumulative, energy, x, 0, energy->width);
    }
    
    return cumulative;
}

static size_t * buildMinEnergyPath(const matrix_t * cumulative, size_t real_width) {
    size_t start = 0;
    size_t * path = malloc(sizeof(size_t) * cumulative->height);

    if (!path) {
        return NULL;
    }

    for (size_t x = 1; x < real_width; x++) {
        if (getElementMatrix(cumulative, start, 0) > getElementMatrix(cumulative, x, 0)) {
            start = x;
        }
    }

    path[0] = start;

    size_t x = start;
    for (size_t y = 1; y < cumulative->height; y++) {
        size_t next_x = x;
        int min_value = getElementMatrix(cumulative, x, y);

        if (x > 0) {
            int left = getElementMatrix(cumulative, x - 1, y);
            
            if (left < min_value) {
                next_x = x - 1;
                min_value = left;
            }
        }
        
        if (x < real_width - 1) {
            int right = getElementMatrix(cumulative, x + 1, y);

            if (right < min_value) {
                next_x = x + 1;
                min_value = right;
            }
        }

        path[y] = next_x;
        x = next_x;
    }

    return path;
}

static void removePathImage(const PNMImage * image, size_t * path) {
    for (size_t y = 0; y < image->height; y++) {
        size_t p = path[y];
        for (size_t x = 0; x < image->width - 1; x++) {
            if (x >= p) {
                PNMPixel pixel = getPixelPNM(image, x + 1, y);
                setPixelImagePNM(image, x, y, pixel);
            }
        }
    }
}

static void removePathEnergyMatrix(const PNMImage * image, const matrix_t * energy, size_t new_width, size_t * path) {
    for (size_t y = 0; y < energy->height; y++) {
        size_t pixel_removed = path[y];
        if (pixel_removed > 0) {
            // pixel left to pixel_removed
            setElementMatrix(energy, pixel_removed - 1, y, getEnergyPixel(image, pixel_removed - 1, y));
        }

        if (pixel_removed < new_width) {
            // current pixel_removed
            setElementMatrix(energy, pixel_removed, y, getEnergyPixel(image, pixel_removed, y));
        }

        if (pixel_removed < new_width - 1) {
            // pixel right to pixel_removed
            setElementMatrix(energy, pixel_removed + 1, y, getEnergyPixel(image, pixel_removed + 1, y));
        }

        // offset end of line by 1
        for (size_t x = pixel_removed + 2; x < new_width - 1; x++) {
            setElementMatrix(energy, x, y, getElementMatrix(energy, x + 1, y));
        }
    }
}

static void removePathCumulativeMatrix(const matrix_t * cumulative, const matrix_t * energy, size_t new_width, size_t * path) {
    size_t end_path = path[cumulative->height - 1];
    size_t left_end_last_line;
    size_t right_end_last_line;

    for (size_t i = 0; i < cumulative->height; i++) {
        size_t y = cumulative->height - 1 - i;
        
        size_t left_end_line;
        if (end_path >= i + 1) {
            left_end_line = end_path - (i + 1);
        } else {
            left_end_line = 0;
        }
        
        size_t right_end_line;
        if (end_path + i + 1 < new_width - 1) {
            right_end_line = end_path + i + 1;
        } else {
            right_end_line = new_width - 1;
        }
        
        for (size_t x = left_end_line; x < right_end_line; x++) {
            setElementMatrix(cumulative, x, y, -1);
        }

        // offset end of line by 1
        for (size_t x = right_end_line; x < new_width - 1; x++) {
            setElementMatrix(cumulative, x, y, getElementMatrix(cumulative, x + 1, y));
        }

        left_end_last_line = left_end_line;
        right_end_last_line = right_end_line;

    }
    
    for (size_t x = left_end_last_line; x < right_end_last_line; x++) {
        computeCumulativeEnergyPath(cumulative, energy, x, 0, new_width);
    }
}


PNMImage * reduceImageWidth(const PNMImage* image, size_t k) {
    const matrix_t * energy = buildEnergyMatrix(image);
    if (!energy) {
        return NULL;
    }

    const matrix_t * cumulative = buildCumulativeEnergyMatrix(energy);
    if (!cumulative) {
        freeMatrix(energy);
        return NULL;
    }

    for (size_t i = 0; i < k; i++) {
        size_t new_width = image->width - i;

        size_t * path = buildMinEnergyPath(cumulative, new_width);
        if (!path) {
            freeMatrix(energy);
            freeMatrix(cumulative);
            return NULL;
        }

        removePathImage(image, path);
        removePathEnergyMatrix(image, energy, new_width, path);
        removePathCumulativeMatrix(cumulative, energy, new_width, path);

        free(path);
    }
    
    freeMatrix(energy);
    freeMatrix(cumulative);
    

    PNMImage * result = createPNM(image->width - k, image->height);

    for (size_t y = 0; y < result->height; y++) {
        for (size_t x = 0; x < result->width; x++) {
            PNMPixel pixel = getPixelPNM(image, x, y);
            setPixelImagePNM(result, x, y, pixel);
        }
    }

    return result;
}